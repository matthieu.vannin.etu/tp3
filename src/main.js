import Router from './Router';
import data from './data';
import Component from './components/Component.js';
import PizzaList from './pages/PizzaList';
import { doc } from 'prettier';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

document.body.children[1].setAttribute('style', '');
const link = document.querySelector('.newsContainer .closeButton');
link.addEventListener('click', event => {
	document.body.children[1].setAttribute('style', 'display:none');
});

Router.menuElement = document.querySelector('.mainMenu');
